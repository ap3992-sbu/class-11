package sbu.cs.ended.pages;

import javafx.scene.Scene;
import sbu.cs.ended.PageLoader;
import sbu.cs.ended.controller.MainController;

import java.io.IOException;

public class Main {

    private final Scene scene;

    public Main() throws IOException {
        scene = new PageLoader().load("/Main.fxml");
        setListeners();
    }

    private void setListeners() {
        MainController.setListeners(scene);
    }
}
