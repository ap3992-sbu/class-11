package sbu.cs.ended;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class PageLoader {
    private static final int WIDTH = 519;
    private static final int HEIGHT = 400;
    private static Stage stage;

    static void intiStage(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle("My hero academy");
        stage.setResizable(false);
        stage.setWidth(WIDTH);
        stage.setHeight(HEIGHT);
    }


    public Scene load(String url) throws IOException {
        URL resource = getClass().getResource(url);
        System.out.println("resource url to " + url + " is " + resource);
        Parent root = FXMLLoader.load(resource);
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        stage.setScene(scene);
        stage.show();
        return scene;
    }

}