package sbu.cs.ended;


import javafx.application.Application;
import javafx.stage.Stage;
import sbu.cs.ended.pages.Main;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        PageLoader.intiStage(primaryStage);
        new Main();
    }


    public static void main(String[] args) {
        launch(args);
    }
}