package sbu.cs.gui.configuration;

import java.util.ResourceBundle;

/**
 * its better to make it singleton
 */
public class Config {

    private final ResourceBundle config;

    public Config() {
        config = ResourceBundle.getBundle("config");
    }

    public int getIntValue(String key) {
        return Integer.parseInt(config.getString(key));
    }

    public String getStringValue(String key) {
        return config.getString(key);
    }

    public boolean getBooleanValue(String key) {
        return config.getString(key).equalsIgnoreCase("true");
    }
}
