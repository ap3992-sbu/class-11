package sbu.cs.gui.pages;

import javafx.scene.Scene;
import sbu.cs.gui.PageLoader;
import sbu.cs.gui.configuration.Config;
import sbu.cs.gui.controller.HomeController;

import java.io.IOException;

public class Home {

    public Home(Config config) throws IOException {
        Scene scene = new PageLoader().load("/Home.fxml");
        HomeController.setKeyListener(scene);
        HomeController.setVariableConfig(config.getIntValue("step.px"));
    }
}
